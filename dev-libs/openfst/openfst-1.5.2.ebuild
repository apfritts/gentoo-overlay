#!/bin/bash
# $Id$

EAPI="5"

DESCRIPTION="A library for constructing, combining, optimizing, and searching weighted finite-state transducers (FSTs)."
HOMEPAGE="http://www.openfst.org"
SRC_URI="http://www.openfst.org/twiki/pub/FST/FstDownload/${P}.tar.gz"

LICENSE="APACHE-2.0"
SLOT="0"
KEYWORDS="~armv7"

src_configure() {
	econf \
		--enable-compact-fsts \
		--enable-compress \
		--enable-const-fsts \
		--enable-far \
		--enable-linear-fsts \
		--enable-lookahead-fsts \
		--enable-mpdt \
		--enable-ngram-fsts \
		--enable-pdt \
		$(use_enable python)
}

src_compile() {
	emake -j1
}

