# apfritts Gentoo Overlay

This is a collection of ebuilds for things I randomly find useful on the internet.

For up-to-date Layman instructions, see the [Gentoo Wiki](https://wiki.gentoo.org/wiki/Layman#Adding_custom_overlays).

For not up-to-date instructions, install this repo using:

 sudo layman -o https://gitlab.com/apfritts/gentoo-overlay/raw/master/layman.xml -f -a apfritts

